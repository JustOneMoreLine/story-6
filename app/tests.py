from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

from datetime import datetime, date, time, timezone
from .models import Status
from .forms import StatusForm
from story6.settings import BASE_DIR
import os

# models:
# -status 300 char
# -time of status
# forms:
# -input 300
# -submit
# Create your tests here.

#Unit Test
class UnitTest(TestCase):
    
    def test_landing_page(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertTemplateUsed(resp, 'landing.html')
        self.assertContains(resp, "Halo, apa kabar?")
        self.assertEqual(resp.status_code, 200)
    
    def test_landing_page_redirect(self):
        url = reverse('index')
        resp = self.client.post(url)
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, url)
    
    def test_models_status(self):
        time = datetime.now()
        status = "Could it be? That we really switch places?"
        model = Status.objects.create(status=status,time=time)
        self.assertEqual(Status.objects.all().count(), 1)
        self.assertEqual(model.getStatus(), status)
        self.assertEqual(model.getTime(), str(time))
        self.assertIsInstance(str(model), str)

    def test_forms_status(self):
        time = datetime.now
        data = {'status': "Is it really that we really switch places?"}
        form = StatusForm(data=data)
        self.assertIsInstance(form, StatusForm)
        self.assertTrue(form.is_valid())

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.headless = True
        self.selenium = webdriver.Firefox(options=options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
    
    def test_status1(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_name('post')
        status.send_keys('Today is gonna be a good day')
        submit.send_keys(Keys.RETURN)

        selenium.implicitly_wait(10)
        selenium.get(self.live_server_url)
        assert 'Today is gonna be a good day' in selenium.page_source

    def test_status2(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_name('post')
        status.send_keys('Coba-coba')
        submit.send_keys(Keys.RETURN)
        
        selenium.implicitly_wait(10)
        selenium.get(self.live_server_url)
        assert 'Coba-coba' in selenium.page_source

    def test_status3(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_name('post')
        status.send_keys('Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon ')
        submit.send_keys(Keys.RETURN)

        selenium.implicitly_wait(10)
        selenium.get(self.live_server_url)
        assert 'Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon Watermelon ' not in selenium.page_source