from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm
from datetime import datetime

# Create your views here.
def index(request):
    if request.method == "GET":
        form = StatusForm
        post = Status.objects.all()
        return render(request, 'landing.html', {"form":form, "data":post})
    if request.method == "POST":
        time = datetime.now()
        form = StatusForm(request.POST)
        if form.is_valid():
            cleanStatus = form.cleaned_data["status"]
            t = Status(status=cleanStatus, time=time)
            t.save()
        return redirect('index')