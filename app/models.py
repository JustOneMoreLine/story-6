from django.db import models
from datetime import datetime

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300, default=None)
    time = models.DateTimeField(default=datetime.now)

    def getStatus(self):
        return str(self.status)
    def getTime(self):
        return str(self.time)
    def __str__(self):
        return str(self.status)